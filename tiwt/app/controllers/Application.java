package controllers;

import models.*;
import play.data.Form;
import play.mvc.*;

import views.html.*;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

public class Application extends Controller {

    public static List<String> categories = Activity.getCategories("conf/categories.csv");

    /**
     * Used to accept or dismiss an activity.
     * Also handles removing users from activities.
     * @return redirects to the created_info page
     */
    @Security.Authenticated(Secured.class)
    public static Result acceptDismiss() {
        Form<AcceptDismiss> acceptDismissForm = Form.form(AcceptDismiss.class).bindFromRequest();
        Long Id = Long.parseLong(acceptDismissForm.get().activity);
        String username = acceptDismissForm.get().username;
        User applicant = User.getByUsername(username);
        String add = acceptDismissForm.get().add;
        User creator = User.getByUsername(request().username());
        if(add.equals("ADD")) {
            creator.addToAccepted(Id, applicant);
            flash("success", "Accepted applicant.");
        }
        if(add.equals("DEL")) {
            applicant.deleteActivity(Id);
            flash("success", "Dismissed applicant.");
        }
        if(add.equals("DELP")) {
            applicant.deleteActivity(Id);
            flash("success", "Dismissed participant.");
        }
        return redirect(routes.Application.createdInfo(Id));
    }

    /**
     * Used to show info about an activity the user was accepted to.
     * @param Id id of the activity
     * @return redirects to the accepted page if the user is not accepted
     * shows the accepted_info page otherwise
     */
    @Security.Authenticated(Secured.class)
    public static Result acceptedInfo(Long Id) {
        Activity activity = Activity.getById(Id);
        User user = User.getByUsername(request().username());
        List<Subscription> accepted = activity.getAccepted();
        if(!activity.isAccepted(user)) {
            flash("error", "You are not accepted.");
            return redirect(routes.Application.listAccepted());
        }
        return ok(accepted_info.render(accepted, activity, Form.form(AcceptDismiss.class)));
    }

    /**
     * Used to show info about an activity the user has applied for.
     * @param Id id of the activity
     * @return redirects to the applied page if the user has not applied
     * shows the applied_info page otherwise
     */
    @Security.Authenticated(Secured.class)
    public static Result appliedInfo(Long Id) {
        Activity activity = Activity.getById(Id);
        User user = User.getByUsername(request().username());
        List<Subscription> accepted = activity.getAccepted();
        List<Subscription> applied = activity.getApplied();
        Integer subscribers = accepted.size() + applied.size();
        if(!activity.isApplied(user)) {
            flash("error", "You have not applied.");
            return redirect(routes.Application.listApplied());
        }
        Subscription subscription = user.getSubscriptionByActivity(activity);
        return ok(applied_info.render(subscription, subscribers, activity, Form.form(AcceptDismiss.class)));
    }

    /**
     * Used to check if a login request is valid.
     * @return redirects to login page if request is invalid
     * shows tiwt page otherwise
     */
    public static Result authenticate() {
        Form<Login> loginForm;
        try {
            loginForm = Form.form(Login.class).bindFromRequest();
        } catch (RuntimeException e) {
            return index();
        }
        if (loginForm.hasErrors()) {
            return badRequest(login.render(loginForm));
        } else {
            String currentUsername = loginForm.get().username;
            session().clear();
            session("username", currentUsername);
            return redirect(routes.Application.index());
        }
    }

    /**
     * Shows the browsing page.
     * @return redirects to the raw page if there are no new
     * activities for the current user, shows the browse page otherwise
     */
    @Security.Authenticated(Secured.class)
    public static Result browse() {
        String category = session().get("category");
        User user = User.getByUsername(request().username());
        Activity activity = Activity.getForBrowsing(category, user);
        if(activity == null) {
            flash("error", "No more activites in Queue.");
            return createRaw(category);
        }
        return ok(browse.render(activity, activity.getCreator(), Form.form(NextActivity.class)));
    }

    /**
     * Used to show info about an activity the user has created.
     * @param Id id of the activity
     * @return redirects to the created page if the user is not
     * the creator, shows the created_info page otherwise
     */
    @Security.Authenticated(Secured.class)
    public static Result createdInfo(Long Id) {
        Activity activity = Activity.getById(Id);
        User user = User.getByUsername(request().username());
        if(!activity.isCreator(user)) {
            flash("error", "You are not the creator.");
            return redirect(routes.Application.listCreated());
        }
        List<Subscription> applied = activity.getApplied();
        List<Subscription> accepted = activity.getAccepted();
        return ok(created_info.render(applied, accepted, activity, Form.form(AcceptDismiss.class)));
    }

    /**
     * Used to check if a create activity request is valid.
     * @return redirects to raw page if request is invalid
     * shows created page otherwise
     */
    @Security.Authenticated(Secured.class)
    public static Result createNew() {
        String category = session().get("category");
        try {
            Form<NewActivity> activityForm = Form.form(NewActivity.class).bindFromRequest();
            if (activityForm.hasErrors()) {
                List<Location> locations = Location.getListByCategory(category);
                return badRequest(raw.render(category, locations, activityForm));
            } else {
                User user = User.getByUsername(request().username());
                String title = activityForm.get().title;
                String description = activityForm.get().description;
                Long locationId = Long.parseLong(activityForm.get().location);
                Location location = Location.getById(locationId);
                user.createActivity(title, category, description, location);
                return redirect(routes.Application.listCreated());
            }
        } catch (Exception e) {
            flash("error", "Please select a location.");
            return createRaw(category);
        }
    }

    /**
     * Shows page for creating a new activity.
     * @param category category for new activity
     * @return shows the raw page
     */
    @Security.Authenticated(Secured.class)
    public static Result createRaw(String category) {
        session().put("category",category);
        List<Location> locs = Location.getListByCategory(category);
        return ok(raw.render(category, locs, Form.form(NewActivity.class)));
    }

    /**
     * Used to delete an activity.
     * @param Id id of activity
     * @return shows the corresponding page of
     * association of user to activity
     */
    @Security.Authenticated(Secured.class)
    public static Result deleteActivity(Long Id) {
        User user = User.getByUsername(request().username());
        Boolean wasAccepted = user.deleteActivity(Id);
        flash("success", "Deleted activity.");
        if(wasAccepted == null) {
            return redirect(routes.Application.listCreated());
        }
        if(wasAccepted) {
            return redirect(routes.Application.listAccepted());
        } else {
            return redirect(routes.Application.listApplied());
        }
    }

    /**
     * Used to check if a edit profile request is valid.
     * @return redirects to edit_profile page and tells
     * the user if request was successfull
     */
    @Security.Authenticated(Secured.class)
    public static Result editProfile() {
        Form<EditProfile> editForm = Form.form(EditProfile.class).bindFromRequest();
        User user = User.getByUsername(request().username());
        if (editForm.hasErrors()) {
            return badRequest(edit_profile.render(user, editForm));
        }
        String firstName = editForm.get().firstName;
        String lastName = editForm.get().lastName;
        Integer day = editForm.get().day;
        Integer month = editForm.get().month;
        Integer year = editForm.get().year;
        String aboutMe = editForm.get().aboutMe;
        String toggleShowRejected = editForm.get().showRejected;
        String newPassword = editForm.get().newPassword;
        Boolean changed = user.editProfile(firstName, lastName, day, month, year, aboutMe, toggleShowRejected, newPassword);
        if(changed) {
            flash("success", "Changed profile.");
            return redirect(routes.Application.profile(request().username()));
        }
        flash("error", "No changes.");
        return redirect(routes.Application.profile(request().username()));
    }

    /**
     * Shows the index.
     * @return shows index
     */
    @Security.Authenticated(Secured.class)
    public static Result index() {
        return tiwt();
    }

    /**
     * Lists accepted activities.
     * @return shows accepted page
     */
    @Security.Authenticated(Secured.class)
    public static Result listAccepted() {
        User user = User.getByUsername(request().username());
        return ok(accepted.render(user));
    }

    /**
     * Lists applied activities.
     * @return shows applied page
     */
    @Security.Authenticated(Secured.class)
    public static Result listApplied() {
        User user = User.getByUsername(request().username());
        return ok(applied.render(user));
    }

    /**
     * Lists created activities.
     * @return shows created page
     */
    @Security.Authenticated(Secured.class)
    public static Result listCreated() {
        User user = User.getByUsername(request().username());
        return ok(created.render(user));
    }

    /**
     * Shows login.
     * @return shows login page
     */
    public static Result login() { return ok(login.render(Form.form(Login.class))); }

    /**
     * Clears session and redirects to login.
     * @return redirects to login
     */
    public static Result logout() {
        session().clear();
        flash("success", "You have been logged out.");
        return redirect(routes.Application.login());
    }

    /**
     * Lists activity options.
     * @return shows activities page
     */
    @Security.Authenticated(Secured.class)
    public static Result myActivities() {
        return ok(activities.render());
    }

    /**
     * Used to validate user input while browsing.
     * Tells the user if he dismissed or applied for an activity.
     * @return redirects to browse page
     */
    @Security.Authenticated(Secured.class)
    public static Result nextActivity() {
        Form<NextActivity> nextForm = Form.form(NextActivity.class).bindFromRequest();
        String add = nextForm.get().add;
        User user = User.getByUsername(request().username());
        Long Id = Long.parseLong(nextForm.get().activity);
        if(add.equals("ADD")) {
            user.addToApplied(Id);
            flash("yes", "Applied for activity.");
        }
        if(add.equals("DEL")) {
            user.addToRejected(Id);
            flash("no", "Dismissed activity.");
        }
        return redirect(routes.Application.browse());
    }

    /**
     * Used to post a message to a chat.
     * @return redirects to chat page
     */
    @Security.Authenticated(Secured.class)
    public static Result postMessage() {
        Form<PostMessage> postMessageForm = Form.form(PostMessage.class).bindFromRequest();
        Long Id = Long.parseLong(session().get("activity"));
        String content = postMessageForm.get().message;
        Activity activity = Activity.getById(Id);
        activity.addMessage(request().username(), content);
        return redirect(routes.Application.showChat(Id));
    }

    /**
     * Shows the profile page of a user.
     * @param username user for profile
     * @return shows edit_profile page if request was
     * sent by user owning the account, shows normal
     * profile page otherwise
     */
    @Security.Authenticated(Secured.class)
    public static Result profile(String username) {
        User user = User.getByUsername(username);
        if(username.equals(request().username())) {
            return ok(edit_profile.render(user, Form.form(EditProfile.class)));
        }
        return ok(profile.render(user));
    }

    /**
     * Shows register page.
     * @return shows register page
     */
    public static Result register() {
        return ok(register.render(Form.form(Register.class)));
    }

    /**
     * Used to validate user input for a new user.
     * Sends message saying what was wrong or to confirm success.
     * @return redirects to login page if request was valid
     * shows register page otherwise
     */
    public static Result signUp() {
        Form<Register> registerForm;
        try{
            registerForm = Form.form(Register.class).bindFromRequest();
        } catch (RuntimeException e){
            registerForm = Form.form(Register.class);
            registerForm.reject("Invalid input.");
            return badRequest(register.render(registerForm));
        }
        if (registerForm.hasErrors()) {
            return badRequest(register.render(registerForm));
        } else {
            String username = registerForm.get().username;
            String firstName = registerForm.get().firstName;
            String lastName = registerForm.get().lastName;
            Integer day = registerForm.get().day;
            Integer month = registerForm.get().month;
            Integer year = registerForm.get().year;
            String password = registerForm.get().password;
            User newUser = new User(username, firstName, lastName, day, month, year, password);
            User.create(newUser);
            flash("success", "Signed up successfully.");
            return redirect(routes.Application.login());
        }
    }

    /**
     * Shows chat of an activity.
     * @param Id id of activity owning chat
     * @return shows the chat page if the user is
     * allowed to see it redirects otherwise
     */
    @Security.Authenticated(Secured.class)
    public static Result showChat(Long Id) {
        Activity activity = Activity.getById(Id);
        User user = User.getByUsername(request().username());
        if(!activity.isAccepted(user) && !activity.isCreator(user)) {
            flash("error", "You have no right to be here.");
            return redirect(routes.Application.myActivities());
        }
        session().put("activity", Id.toString());
        List<Message> messages = activity.getChat();
        return ok(chat.render(messages, Form.form(PostMessage.class)));
    }

    /**
     * Shows the tiwt page.
     * @return redirects to login if there is no
     * user in session, shows tiwt page otherwise
     */
    @Security.Authenticated(Secured.class)
    public static Result tiwt() {
        User user = User.getByUsername(request().username());
        if(user == null) {
            return redirect(routes.Application.login());
        }
        return ok(tiwt.render(categories, user));
    }

    /**
     * Form for accepting or dismissing an applicant.
     */
    public static class AcceptDismiss {
        public String activity;
        public String username;
        public String add;
    }

    /**
     * Form for editing one's profile.
     */
    public static class EditProfile {
        public String firstName;
        public String lastName;
        public Integer day;
        public Integer month;
        public Integer year;
        public String aboutMe;
        public String showRejected;
        public String newPassword;
        public String password;

        public String validate() {
            String username = request().username();
            if(User.authenticate(username, password) == null) {
                return "Wrong password.";
            }
            return null;
        }
    }

    /**
     * Form for logging in.
     */
    public static class Login {
        public String username;
        public String password;

        public String validate() {
            if(User.authenticate(username, password) == null) {
                return "Invalid user or password.";
            }
            return null;
        }
    }

    /**
     * Form for creating a new activity.
     */
    public static class NewActivity {
        public String title;
        public String description;
        public String location;

        public String validate() {
            if(title.equals("Enter title...") || title.equals("")) {
                return "Please enter a title.";
            }
            if(description.equals("Enter description...") || description.equals("")) {
                return "Please enter a description.";
            }
            if(title.length() > 20) {
                return "Title is too long (20 max).";
            }
            if(description.length() > 120) {
                return "Description is too long (120 max).";
            }
            return null;
        }
    }

    /**
     * Form for applying for or dismissing an activity.
     */
    public static class NextActivity {
        public String activity;
        public String add;
    }

    /**
     * Form for posting a message.
     */
    public static class PostMessage {
        public String message;
    }

    /**
     * Form for registering.
     */
    public static class Register {
        public String username;
        public String firstName;
        public String lastName;
        public Integer day;
        public Integer month;
        public Integer year;
        public String password;

        public String validate() {
            String success = User.authenticateSignUp(username, firstName, lastName, day, month, year, password);
            return success;
        }
    }
}