package controllers;

import play.mvc.*;
import play.mvc.Http.*;

/**
 * Created by csaq6155 on 20/01/15.
 */
public class Secured extends Security.Authenticator {

    @Override
    public String getUsername(Context ctx) {
        return ctx.session().get("username");
    }

    @Override
    public Result onUnauthorized(Context ctx) {
        return redirect(routes.Application.login());
    }
}
