package models;

import com.avaje.ebean.common.BeanList;
import org.joda.time.IllegalFieldValueException;
import org.joda.time.LocalDate;
import org.joda.time.Years;
import org.mindrot.jbcrypt.BCrypt;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by csaq6155 and csaq7307 on 13/01/15.
 * User of the application. The user can apply for activities,
 * accept other users to activities he created and create
 * activities.
 */
@Entity
public class User extends Model {

    @Id
    public String username;

    public String firstName;
    public String lastName;
    public LocalDate birthDate;
    public String aboutMe;
    public Boolean showRejected;
    public String password;

    @OneToMany(mappedBy = "creator", cascade = CascadeType.ALL)
    public List<Activity> createdActivities;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    public List<Subscription> subscriptions;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    public List<Rejection> rejections;

    public User(String username, String firstName, String lastName, Integer day, Integer month, Integer year, String password) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = new LocalDate(year, month, day);
        this.aboutMe = "";
        this.showRejected = false;
        this.password = BCrypt.hashpw(password, BCrypt.gensalt());
        this.createdActivities = new BeanList<Activity>();
        this.subscriptions = new BeanList<Subscription>();
        this.rejections = new BeanList<Rejection>();
    }

    /**
     * Method used to authenticate a user who is trying to sign in.
     * @param username The entered username.
     * @param password The entered password.
     * @return
     */
    public static User authenticate(String username, String password) {
        User user =  find.where().eq("username", username).findUnique();
        if(user != null && BCrypt.checkpw(password, user.password)) {
            return user;
        }
        return null;
    }

    /**
     * Method used to check the input of the register form.
     * @param username
     * @param firstName
     * @param lastName
     * @param day
     * @param month
     * @param year
     * @param password
     * @return
     */
    public static String authenticateSignUp(String username, String firstName, String lastName, Integer day, Integer month, Integer year, String password) {
        if(find.where().eq("username", username).findUnique() != null) {
            return "Username already exists.";
        }
        if(username == "") {
            return "Username must not be empty.";
        }
        if(firstName == "") {
            return "Firstname must not be empty.";
        }
        if(lastName == "") {
            return "Lastname must not be empty.";
        }
        String dateCheck = isDateInvalid(year, month, day);
        if(dateCheck != null) {
            return dateCheck;
        }
        if(isPasswordInvalid(password)) {
            return "Password must be at least 8 letters long.";
        }
        return null;
    }

    /**
     * Method used to check if a Date is valid.
     * @param year Year which should be checked.
     * @param month Month which should be checked.
     * @param day Day which should be checked.
     * @return Null if date is valid, String explaining why not otherwise.
     */
    public static String isDateInvalid(Integer year, Integer month, Integer day) {
        if(year == 2525) {
            return "Is man still alive?";
        }
        try {
            LocalDate date = new LocalDate(year, month, day);
            LocalDate now = new LocalDate();
            int age = Years.yearsBetween(date, now).getYears();
            if(age < 0) {
                return "Wait til you are born.";
            }
            if(age <= 16) {
                return "You are too young.";
            }
            if(age >= 500 && age <= 550) {
                return "Did you discover America?";
            }
            if(age >= 210 && age <= 230) {
                return "Liberté, égalité, fraternité!";
            }
            if(age >= 122) {
                return "You are the oldest person in the world. (too old for us)";
            }
        } catch (IllegalFieldValueException e) {
            return "Date is invalid";
        }
        return null;
    }

    /**
     * Method used to check if a password is save by minimal standards.
     * @param password
     * @return
     */
    public static Boolean isPasswordInvalid(String password) {
        return (password.length() < 8);
    }

    public Boolean editProfile(String firstName, String lastName, Integer day, Integer month, Integer year, String aboutMe, String toggleShowRejected, String newPassword) {
        Boolean flag = false;
        if (firstName != null && !(firstName.equals(this.firstName)) && firstName != "") {
            this.firstName = firstName;
            flag = true;
        }
        if (lastName != null && !(lastName.equals(this.lastName)) && lastName != "") {
            this.lastName = lastName;
            flag = true;
        }
        if(year != null && month != null && day != null) {
            String dateCheck = isDateInvalid(year, month, day);
            if (dateCheck == null) {
                setBirthDate(year, month, day);
                flag = true;
            }
        }
        if(aboutMe != null && !(aboutMe.equals(this.aboutMe)) && (aboutMe.length() <= 120)) {
            this.aboutMe = aboutMe;
            flag = true;
        }
        if(toggleShowRejected != null && toggleShowRejected.equals("toggle")) {
            toggleShowRejected();
            flag = true;
        }
        if (newPassword != null) {
            if (!isPasswordInvalid(newPassword)) {
                this.password = BCrypt.hashpw(newPassword, BCrypt.gensalt());
                flag = true;
            }
        }
        this.update();
        return flag;
    }

    /**
     * Used for DB access.
     */
    public static Finder<String,User> find = new Finder(
            String.class, User.class
    );

    /**
     * Gets all users from DB.
     * @return All users in DB
     */
    public static List<User> all() {
        return find.all();
    }

    /**
     * Stores new user in DB.
     * @param user user to store
     */
    public static void create(User user) {
        user.save();
    }

    /**
     * Fetches a user from DB.
     * @param username user to fetch
     * @return user from DB
     */
    public static User getByUsername(String username) {
        if(username == null) {
            return null;
        }
        return find.byId(username);
    }

    //Getters and setters
    public String getUsername() {
        return this.username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public void setBirthDate(Integer year, Integer month, Integer day) { this.birthDate = new LocalDate(year, month, day); }
    public String getPassword() {
        return this.password;
    }
    public Boolean getShowRejected() { return this.showRejected; }
    public void setPassword(String password) { this.password = password; }
    public List<Activity> getCreatedActivities() {
        return createdActivities;
    }
    public List<Subscription> getSubscriptions() {
        return subscriptions;
    }

    /**
     * Calculates and returns the users age.
     * @return the user's age
     */
    public String getAge() {
        LocalDate now = new LocalDate();
        if(this.birthDate == null) {
            this.birthDate = new LocalDate();
        }
        Years age = Years.yearsBetween(this.birthDate, now);
        return Integer.toString(age.getYears());
    }

    /**
     * Adds an activity to the user's acceptedActivites list.
     * @param Id id of the activity which should be added
     */
    public void addToAccepted(Long Id, User applicant) {
        Activity activity = Activity.getById(Id);
        User creator = activity.getCreator();
        if(creator.equals(this) && !creator.equals(applicant)) {
            BeanList<Subscription> subs = (BeanList<Subscription>) applicant.getSubscriptions();
            for(Subscription sub : subs) {
                if(sub.getActivity().equals(activity)) {
                    sub.setAccepted();
                    sub.update();
                    break;
                }
            }
        }
    }

    /**
     * Adds an activity to the user's appliedActivites list.
     * @param Id id of the activity which should be added
     */
    public void addToApplied(Long Id) {
        Activity activity = Activity.getById(Id);
        if(activity != null) {
            User creator = activity.getCreator();
            if (!creator.equals(this)) {
                Subscription subscription = new Subscription(activity, this);
                this.subscriptions.add(subscription);
                activity.addSubscription(subscription);
                Subscription.create(subscription);
            }
        }
    }

    /**
     * Adds an activity to the user's rejections.
     * @param Id id of the activity which should be added
     */
    public void addToRejected(Long Id) {
        Activity activity = Activity.getById(Id);
        if(activity != null) {
            User creator = activity.getCreator();
            if (!creator.equals(this)) {
                Rejection rejection = new Rejection(activity, this);
                this.rejections.add(rejection);
                activity.addRejection(rejection);
                Rejection.create(rejection);
            }
        }
    }

    /**
     * Creates a new Activity with user as created.
     * @param category category of activity
     * @param description description of activity
     */
    public void createActivity(String title, String category, String description, Location location) {
        Activity activity = new Activity(title, category, description, this, location);
        activity.save();
    }

    /**
     * Deletes an activity associated with the user.
     * Checks the user is the creator and, if so,
     * deletes the activity from the DB.
     * @param Id id of activity to delete
     */
    public Boolean deleteActivity(Long Id) {
        Activity activity = Activity.getById(Id);
        User creator = activity.getCreator();
        if(creator.equals(this)) {
            Activity.delete(Id);
            return null;
        } else {
            for(Subscription sub : this.subscriptions) {
                if(activity.equals(sub.getActivity())) {
                    Boolean accepted = sub.isAccepted();
                    sub.delete();
                    return accepted;
                }
            }
        }
        return null;
    }

    /**
     * Returns activities the user was accepted for.
     * @return list of activities
     */
    public List<Activity> getAccepted() {
        BeanList<Activity> activities = new BeanList<Activity>();
        for(Subscription sub : this.subscriptions) {
            if(sub.isAccepted()) {
                activities.add(sub.getActivity());
            }
        }
        return activities;
    }

    /**
     * Returns activities the user has applied for.
     * @return list of activities
     */
    public List<Activity> getApplied() {
        BeanList<Activity> activities = new BeanList<Activity>();
        for(Subscription sub : this.subscriptions) {
            if(!sub.isAccepted()) {
                activities.add(sub.getActivity());
            }
        }
        return activities;
    }

    /**
     * Returns the subscription of a certain activity.
     * @param activity activity corresponding to subscription.
     * @return subscription if can be found, null otherwise
     */
    public Subscription getSubscriptionByActivity(Activity activity) {
        for(Subscription subscription : this.subscriptions) {
            if(activity.equals(subscription.getActivity())) {
                return subscription;
            }
        }
        return null;
    }

    /**
     * Checks if the user has rejected a certain activity
     * @param activity activity to check
     * @return true if the activity was rejected, false otherwise
     */
    public Boolean hasRejected(Activity activity) {
        for(Rejection rejection : this.rejections) {
            if(rejection.getActivity().equals(activity)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if the user has subscribed to a certain activity
     * @param activity activity to check
     * @return true if the user is subscribed, false otherwise
     */
    public Boolean hasSubscribed(Activity activity) {
        for(Subscription subscription : this.subscriptions) {
            if(subscription.getActivity().equals(activity)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if the user created any activites.
     * @return true if the user has not created any activities, false otherwise
     */
    public Boolean isCreatedEmpty() {
        return this.createdActivities.isEmpty();
    }

    /**
     * Checks if the user has any subscriptions.
     * @return true if the user has subscriptions, false otherwise
     */
    public Boolean noSubscriptions() {
        return this.subscriptions.isEmpty();
    }

    /**
     * Toggles if rejected activities should be shown while browsing.
     */
    public void toggleShowRejected() { this.showRejected = !(this.showRejected); }
}
