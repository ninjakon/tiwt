package models;

import com.avaje.ebean.Expr;
import com.avaje.ebean.common.BeanList;
import play.db.ebean.Model;

import javax.persistence.*;
import java.io.*;
import java.util.List;

/**
 * Created by csaq6155 on 15/01/15.             *
 * Activities can be managed by users. Users    *
 * can create an activity, apply for one or     *
 * get accepted to one. Each activity has its   *
 * own chat to which the users can post messages*
 */
@Entity
public class Activity extends Model {

    @Id
    public Long Id;

    public String title;
    public String category;
    public String description;

    @ManyToOne
    public User creator;

    @ManyToOne
    public Location location;

    @OneToMany(mappedBy = "activity", cascade = CascadeType.ALL)
    public List<Subscription> subscriptions;

    @OneToMany(mappedBy = "activity", cascade = CascadeType.ALL)
    public List<Rejection> rejections;

    @OneToMany(mappedBy = "activity", cascade = CascadeType.ALL)
    public List<Message> chat;

    public Activity(String title, String category, String description, User creator, Location location) {
        this.title = title;
        this.category = category;
        this.description = description;
        this.creator = creator;
        this.location = location;
        this.subscriptions = new BeanList<Subscription>();
        this.rejections = new BeanList<Rejection>();
        this.chat = new BeanList<Message>();
    }

    /**
     * Used for DB access.
     */
    public static Finder<Long,Activity> find = new Finder(
            Long.class, Activity.class
    );

    /**
     * Gets all activities from DB.
     * @return all activities from DB
     */
    public static List<Activity> all() {
        return find.all();
    }

    /**
     * Stores a new activity in DB.
     * @param activity
     */
    public static void create(Activity activity) {
        activity.save();
    }

    /**
     * Deletes an activity from DB.
     * @param Id id of activity to delete
     */
    public static void delete(Long Id) {
        find.ref(Id).delete();
    }

    /**
     * Fetches an activity from the DB
     * @param Id
     * @return
     */
    public static Activity getById(Long Id) {
        return find.byId(Id);
    }

    //Getters
    public Long getId() { return this.Id; }
    public String getCategory() { return this.category; }
    public String getDescription() { return this.description; }
    public User getCreator() { return this.creator; }
    public List<Message> getChat() { return this.chat; }

    /**
     * Posts a message to the activities chat.
     * @param username user which posts the message
     * @param content content of the message
     */
    public void addMessage(String username, String content) {
        Message message = new Message(this, username, content);
        this.chat.add(message);
        message.save();
    }

    /**
     * Adds a rejection to the activity.
     * @param rejection rejection to add
     */
    public void addRejection(Rejection rejection) { this.rejections.add(rejection); }

    /**
     * Adds a subscription to the activity.
     * @param subscription subscription to add
     */
    public void addSubscription(Subscription subscription) {
        this.subscriptions.add(subscription);
    }

    /**
     * Returns subscriptions of accepted users.
     * @return list of subscriptions
     */
    public List<Subscription> getAccepted() {
        List<Subscription> accepted = new BeanList<Subscription>();
        for(Subscription sub : this.subscriptions) {
            if(sub.isAccepted()) {
                accepted.add(sub);
            }
        }
        return accepted;
    }

    /**
     * Returns subscriptions of applicants.
     * @return list of subscriptions
     */
    public List<Subscription> getApplied() {
        List<Subscription> applied = new BeanList<Subscription>();
        for(Subscription sub : this.subscriptions) {
            if(!sub.isAccepted()) {
                applied.add(sub);
            }
        }
        return applied;
    }

    /**
     * Fetches all activities of a certain category from DB.
     * @param category category of wanted activities
     * @return activities with category
     */
    public static List<Activity> getByCategory(String category) {
        List<Activity> activities = (BeanList<Activity>) find.where().ilike("category", category).findList();
        return activities;
    }

    /**
     * Fetches all activities of a certain category from DB
     * and filters the ones which where made by a certain creator.
     * @param category category of wanted activities
     * @param user user which should be filtered
     * @return activities of certain type
     */
    public static Activity getForBrowsing(String category, User user) {
        String username = user.getUsername();
        BeanList<Activity> activities = (BeanList<Activity>) find.where().ilike("category", category).not(Expr.ilike("creator", username)).findList();
        if(user.getShowRejected()) {
            for (Activity activity : activities) {
                if (!user.hasSubscribed(activity)) {
                    return activity;
                }
            }
        } else {
            for (Activity activity : activities) {
                if (!user.hasSubscribed(activity) && !user.hasRejected(activity)) {
                    return activity;
                }
            }
        }
        return null;
    }

    /**
     * Checks if a user was accepted for that activity.
     * @param user user to check
     * @return true if user was accepted, false otherwise
     */
    public Boolean isAccepted(User user) {
        for(Subscription sub : this.subscriptions) {
            if(user.equals(sub.getUser()) && sub.isAccepted()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if a user has applied for that activity.
     * @param user user to check
     * @return true if user has applied, false otherwise
     */
    public Boolean isApplied(User user) {
        for(Subscription sub : this.subscriptions) {
            if(user.equals(sub.getUser()) && !sub.isAccepted()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if a user is the activity's creator.
     * @param user potential creator
     * @return true if user is creator false otherwise
     */
    public Boolean isCreator(User user) {
        return this.creator.equals(user);
    }

    /**
     * Returns name of activity's location.
     * @return name of location
     */
    public String locationName() {
        return this.location.getName();
    }

    /**
     * Returns zip code of activity's location.
     * @return zip code of location
     */
    public String locationZipCode() {
        return this.location.getZipCode().toString();
    }

    /**
     * Returns city of activity's location.
     * @return city of location
     */
    public String locationCity() {
        return this.location.getCity();
    }

    /**
     * Initializes the categories by loading the locations from a xls file.
     * Downloads the file if it can't be found locally.
     * @param dataPath file path of csv file holding the init info
     */
    public static void initCategories(String dataPath) {
        try {
            File file = new File(dataPath);
            FileReader in = new FileReader(file);
            BufferedReader br = new BufferedReader(in);
            String categoryData;
            while ((categoryData = br.readLine()) != null) {
                if((categoryData.charAt(0) != '/') && (categoryData.charAt(1) != '/')) {
                    String[] categoryInfo = categoryData.split(",");
                    String categoryName = categoryInfo[0];
                    String filePath = categoryInfo[1];
                    String urlSource = categoryInfo[2];
                    int rows = Integer.parseInt(categoryInfo[3]);
                    int cols = Integer.parseInt(categoryInfo[4]);
                    int nameIndex = Integer.parseInt(categoryInfo[5]);
                    int zipIndex = Integer.parseInt(categoryInfo[6]);
                    int cityIndex = Integer.parseInt(categoryInfo[7]);
                    ExcelReader reader = new ExcelReader(filePath, urlSource);
                    reader.saveInDB(categoryName, rows, cols, nameIndex, zipIndex, cityIndex);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Checks the init file for allowed categories and returns them.
     * @param dataPath file path of csv file holding the init info
     * @return list if allowed categories
     */
    public static List<String> getCategories(String dataPath) {
        List<String> categories = new BeanList<String>();
        try {
            File file = new File(dataPath);
            FileReader in = new FileReader(file);
            BufferedReader br = new BufferedReader(in);
            String categoryData;
            while ((categoryData = br.readLine()) != null) {
                if((categoryData.charAt(0) != '/') && (categoryData.charAt(1) != '/')) {
                    String[] categoryInfo = categoryData.split(",");
                    String categoryName = categoryInfo[0];
                    categories.add(categoryName);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return categories;
    }
}
