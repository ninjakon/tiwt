package models;

import com.avaje.ebean.common.BeanList;
import play.db.ebean.Model;
import javax.persistence.Id;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.Comparator;
import java.util.List;

/**
 * Created by csaq6155 on 22/01/15.
 * Location holds the info of a certain location.
 */
@Entity
public class Location extends Model {

    @Id
    public Long Id;

    public String name;
    public String category;
    public Integer zipCode;
    public String city;

    @OneToMany(mappedBy = "location")
    public List<Activity> activities;

    public Location(String category, String name, Integer zipCode, String city) {
        this.category = category;
        this.name = name;
        this.zipCode = zipCode;
        this.city = city;
    }

    //Getters
    public String getName() { return this.name; }
    public Integer getZipCode() { return this.zipCode; }
    public String getCity() { return this.city; }

    /**
     * Used for DB.
     */
    public static Finder<Long,Location> find = new Finder(
            Long.class, Location.class
    );

    /**
     * Fetches all locations from DB.
     * @return all locations in DB
     */
    public static List<Location> all() {
        return find.all();
    }

    /**
     * Stores a new location in DB
     * @param location location to store
     */
    public static void create(Location location) {
        location.save();
    }

    /**
     * Fetches a location from DB.
     * @param Id id of wanted location
     * @return location from DB
     */
    public static Location getById(Long Id) {
        return find.byId(Id);
    }

    /**
     * Fetches all locations from DB
     * and returns them as a sorted list.
     * @return sorted List of locations
    */
    public static List<Location> getList() {
        BeanList<Location> locations = (BeanList<Location>) find.all();
        locations.sort(comparator);
        return locations;
    }

    /**
     * Fetches locations of a certain category from the DB.
     * @param category category of wanted locations
     * @return list of locations
     */
    public static List<Location> getListByCategory(String category) {
        BeanList<Location> locations = (BeanList<Location>) find.where().ilike("category", category).findList();
        locations.sort(comparator);
        return locations;
    }

    /**
     * Compares name of locations.
     */
    public static Comparator<Location> comparator = new Comparator<Location>() {
        @Override
        public int compare(Location l1, Location l2) {
            String name1 = l1.name;
            String name2 = l2.name;
            return name1.compareTo(name2);
        }
    };
}
