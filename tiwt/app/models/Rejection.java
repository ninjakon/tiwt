package models;

import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.List;

/**
 * Created by csaq6155 on 23/01/15.
 * A rejection is created when a user rejects an activity.
 * The rejection holds the activity and the user.
 */
@Entity
public class Rejection extends Model {

    @Id
    Long Id;

    @ManyToOne
    public Activity activity;

    @ManyToOne
    public User user;

    public Rejection(Activity activity, User user) {
        this.activity = activity;
        this.user = user;
    }

    /**
     * Used for DB access.
     */
    public static Finder<Long, Rejection> find = new Finder(
            Long.class, Rejection.class
    );

    /**
     * Gets all rejections from DB.
     * @return all rejections from DB
     */
    public static List<Rejection> all() {
        return find.all();
    }

    /**
     * Stores a new rejection in DB.
     * @param rejection rejection to be stored
     */
    public static void create(Rejection rejection) {
        rejection.save();
    }

    /**
     * Deletes a rejection from DB.
     * @param Id id of rejection to delete
     */
    public static void delete(Long Id) {
        find.ref(Id).delete();
    }

    /**
     * Fetches a rejection from the DB
     * @param Id id of wanted rejection
     * @return wanted rejection
     */
    public static Rejection getById(Long Id) {
        return find.byId(Id);
    }

    //Getters
    public Activity getActivity() {
        return this.activity;
    }
    public User getUser() {
        return this.user;
    }
}
