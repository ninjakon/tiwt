package models;

import play.db.ebean.Model;

import javax.persistence.*;
import java.util.List;

import org.joda.time.LocalDate;

/**
 * Created by csaq6155 on 23/01/15.
 * A Subscription is created when a user applies for an activity.
 * The subscription holds the activity, the user, the date the
 * user applied and the date he was accepted. The bool accepted
 * is set true when the creator accepts the applicant.
 */
@Entity
public class Subscription extends Model {

    @Id
    Long Id;

    @ManyToOne
    public Activity activity;

    @ManyToOne
    public User user;

    public LocalDate dateApplied;
    public Boolean accepted;
    public LocalDate dateAccepted;

    public Subscription(Activity activity, User user) {
        this.activity = activity;
        this.user = user;
        dateApplied = new LocalDate();
        accepted = false;
    }

    /**
     * Used for DB access.
     */
    public static Finder<Long,Subscription> find = new Finder(
            Long.class, Subscription.class
    );

    /**
     * Gets all subscriptions from DB.
     * @return all subscriptions from DB
     */
    public static List<Subscription> all() {
        return find.all();
    }

    /**
     * Stores a new subscription in DB.
     * @param subscription subscription to be stored
     */
    public static void create(Subscription subscription) {
        subscription.save();
    }

    /**
     * Deletes an subscription from DB.
     * @param Id id of subscription to delete
     */
    public static void delete(Long Id) {
        find.ref(Id).delete();
    }

    /**
     * Fetches an subscription from the DB
     * @param Id id of wanted subscription
     * @return wanted subscription
     */
    public static Subscription getById(Long Id) {
        return find.byId(Id);
    }

    //Getters
    public Activity getActivity() {
        return this.activity;
    }
    public User getUser() {
        return this.user;
    }
    public LocalDate getDateApplied() {
        return this.dateApplied;
    }
    public Boolean isAccepted() {
        return this.accepted;
    }
    public LocalDate getDateAccepted() {
        return this.dateAccepted;
    }

    /*
     * Sets accepted to true the Accepted Date to today.
     */
    public void setAccepted() {
        this.dateAccepted = new LocalDate();
        this.accepted = true;
    }
}
