package models;

import jxl.*;
import jxl.read.biff.BiffException;
import play.twirl.api.Html;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by csaq6155 on 19/01/15.
 */
public class ExcelReader {

    File file;

    /**
     * Used to read the excel file on start.
     * Downloads the file if it can't be found locally.
     * @param filePath local file path
     * @param urlSource file url used if file can't be found
     */
    public ExcelReader(String filePath, String urlSource) {
        file = new File(filePath);
        if(!file.exists()) {
            try {
                URL url = new URL(urlSource);
                URLConnection connection = url.openConnection();
                InputStream in = connection.getInputStream();
                FileOutputStream fos = new FileOutputStream(new File(filePath));
                byte[] buf = new byte[512];
                while (true) {
                    int len = in.read(buf);
                    if (len == -1) {
                        break;
                    }
                    fos.write(buf, 0, len);
                }
                in.close();
                fos.flush();
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Stores a given number of rows and
     * columns to the DB.
     * @param rows number of rows to store, <0 means all
     * @param cols number of columns to store, <0 means all
     */
    public void saveInDB(String category, int rows, int cols, int iName, int iZip, int iCity) {
        try {
            String name = null;
            Integer zipCode = null;
            String city = null;
            WorkbookSettings ws = new WorkbookSettings();
            ws.setEncoding("Cp1252");
            Workbook wb = Workbook.getWorkbook(file, ws);
            Sheet sheet = wb.getSheet(0);
            rows = rows<0 ? sheet.getRows() : rows;
            cols = cols<0 ? sheet.getColumns() : cols;
            for (int i = 3; i < rows; i++) {
                for (int j = 0; j < cols; j++) {
                    Cell cell = sheet.getCell(j, i);
                    CellType type = cell.getType();
                    if (type == CellType.LABEL) {
                        if(j == iName) {
                            name = cell.getContents();
                        } else if(j == iZip) {
                            String zip = cell.getContents();
                            if (zip.equals("")) {
                                zipCode = 0;
                            } else {
                                zipCode = Integer.parseInt(zip);
                            }
                        } else if(j == iCity) {
                            city = cell.getContents();
                        }
                    }
                }
                Location.create(new Location(category, name, zipCode, city));
            }
        } catch(IOException e) {
            e.printStackTrace();
        } catch(BiffException e) {
            e.printStackTrace();
        }
    }

    /**
     * turns the excel to a Html table
     * @param rows number of rows to convert, <0 means all
     * @param cols number of columns to convert, <0 means all
     * @return
     */
    public Html toHtml(int rows, int cols) {
        String content = "<table style=\"width:100%\">";
        try {
            Workbook wb = Workbook.getWorkbook(file);
            Sheet sheet = wb.getSheet(0);
            rows = rows<0 ? sheet.getRows() : rows;
            cols = cols<0 ? sheet.getColumns() : cols;
            for (int i = 0; i < rows; i++) {
                content += "<tr>";
                for (int j = 0; j < cols; j++) {
                    Cell cell = sheet.getCell(j, i);
                    CellType type = cell.getType();
                    if (type == CellType.LABEL) {
                        content += "<td>"+cell.getContents()+"</td>";
                    }
                }
                content += "</tr>";
            }
            content += "</table>";
        } catch(IOException e) {
            content = "File not found.";
        } catch(BiffException e) {
            content = "Biff error.";
        }
        return new Html(content);
    }
}
