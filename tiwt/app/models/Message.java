package models;

import org.joda.time.LocalDate;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.List;

/**
 * Created by csaq6155 on 15/01/15.
 * Messages store their content, their author and the date they were posted.
 */
@Entity
public class Message extends Model {

    @Id
    public Long Id;

    public String content;
    public String author;
    public LocalDate postedDate;

    @ManyToOne
    Activity activity;

    public Message(Activity activity, String author, String content) {
        this.activity = activity;
        this.author = author;
        this.content = content;
        this.postedDate = new LocalDate();
    }

    public static Finder<Long,Message> find = new Finder(
            Long.class, Message.class
    );

    public static List<Message> all() {
        return find.all();
    }

    public static void create(Message message) { message.save(); }

    public static void delete(Long Id) {
        find.ref(Id).delete();
    }

    public static Message getById(Long Id) {
        return find.byId(Id);
    }

    public String getAuthor() { return this.author; }
}
