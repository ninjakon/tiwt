import models.Activity;
import models.Location;
import models.User;
import play.*;

/**
 * Created by csaq6155 on 19/01/15.
 */
public class Global extends GlobalSettings {
    @Override
    public void onStart(Application app) {
        // Check if the database is empty
        if(User.find.findRowCount() == 0) {
            User.create(new User("admin", "John", "Smith", 1, 1, 1970, "password"));
            User.create(new User("dummy", "Jane", "Smith", 1, 1, 1970, "password"));
        }
        if(Location.find.findRowCount() == 0) {
            Activity.initCategories("conf/categories.csv");
        }
    }
}
