# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table activity (
  id                        bigint not null,
  title                     varchar(255),
  category                  varchar(255),
  description               varchar(255),
  creator_username          varchar(255),
  location_id               bigint,
  constraint pk_activity primary key (id))
;

create table location (
  id                        bigint not null,
  name                      varchar(255),
  category                  varchar(255),
  zip_code                  integer,
  city                      varchar(255),
  constraint pk_location primary key (id))
;

create table message (
  id                        bigint not null,
  content                   varchar(255),
  author                    varchar(255),
  posted_date               date,
  activity_id               bigint,
  constraint pk_message primary key (id))
;

create table rejection (
  id                        bigint not null,
  activity_id               bigint,
  user_username             varchar(255),
  constraint pk_rejection primary key (id))
;

create table subscription (
  id                        bigint not null,
  activity_id               bigint,
  user_username             varchar(255),
  date_applied              date,
  accepted                  boolean,
  date_accepted             date,
  constraint pk_subscription primary key (id))
;

create table user (
  username                  varchar(255) not null,
  first_name                varchar(255),
  last_name                 varchar(255),
  birth_date                date,
  about_me                  varchar(255),
  show_rejected             boolean,
  password                  varchar(255),
  constraint pk_user primary key (username))
;

create sequence activity_seq;

create sequence location_seq;

create sequence message_seq;

create sequence rejection_seq;

create sequence subscription_seq;

create sequence user_seq;

alter table activity add constraint fk_activity_creator_1 foreign key (creator_username) references user (username) on delete restrict on update restrict;
create index ix_activity_creator_1 on activity (creator_username);
alter table activity add constraint fk_activity_location_2 foreign key (location_id) references location (id) on delete restrict on update restrict;
create index ix_activity_location_2 on activity (location_id);
alter table message add constraint fk_message_activity_3 foreign key (activity_id) references activity (id) on delete restrict on update restrict;
create index ix_message_activity_3 on message (activity_id);
alter table rejection add constraint fk_rejection_activity_4 foreign key (activity_id) references activity (id) on delete restrict on update restrict;
create index ix_rejection_activity_4 on rejection (activity_id);
alter table rejection add constraint fk_rejection_user_5 foreign key (user_username) references user (username) on delete restrict on update restrict;
create index ix_rejection_user_5 on rejection (user_username);
alter table subscription add constraint fk_subscription_activity_6 foreign key (activity_id) references activity (id) on delete restrict on update restrict;
create index ix_subscription_activity_6 on subscription (activity_id);
alter table subscription add constraint fk_subscription_user_7 foreign key (user_username) references user (username) on delete restrict on update restrict;
create index ix_subscription_user_7 on subscription (user_username);



# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists activity;

drop table if exists location;

drop table if exists message;

drop table if exists rejection;

drop table if exists subscription;

drop table if exists user;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists activity_seq;

drop sequence if exists location_seq;

drop sequence if exists message_seq;

drop sequence if exists rejection_seq;

drop sequence if exists subscription_seq;

drop sequence if exists user_seq;

