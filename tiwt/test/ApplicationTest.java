import models.Activity;
import models.User;
import org.junit.*;

import static org.fest.assertions.Assertions.*;


/**
*
* Simple (JUnit) tests that can call all parts of a play app.
* If you are interested in mocking a whole application, see the wiki for more details.
*
*/
public class ApplicationTest {

    @Test
    public void simpleCheck() {
        int a = 1 + 1;
        assertThat(a).isEqualTo(2);
    }

    static User admin;
    static User applicant;
    static Activity activity;

    @BeforeClass
    public static void initClass() {
        admin = new User("ninjakon", "Arno", "Breitfuss", 14, 4, 1994, "password");
        applicant = new User("vertganti", "Bjoern", "Meusburger", 27, 12, 1993, "passwort");
        admin.createActivity("skiing", "skiing is fun");
        activity = admin.getCreatedActivities().get(0);
    }

    @Test
    public void addActivity() {
        User creator = activity.getCreator();
        assertThat(creator.getUsername()).isEqualTo(admin.getUsername());
    }

    @Test
    public void deleteActivity() {
        assertThat(applicant.getCreatedActivities()).hasSize(0);

        applicant.createActivity("skiing", "skiing is fun");
        Activity activity1 = applicant.getCreatedActivities().get(0);
        assertThat(applicant.getCreatedActivities()).hasSize(1);

        applicant.deleteActivity(activity1);
        assertThat(applicant.getCreatedActivities()).hasSize(0);
    }

    @Test
    public void chat() {
        applicant.addMessage(activity, "hello");
        admin.addMessage(activity, "hi");
        System.out.println(activity.chatToString());
        assertThat(activity.getChat().size()).isEqualTo(2);
    }

    @Test
    public void addToActivity() {
        admin.addToAccepted(activity.getId(), admin);
        assertThat(activity.getAccepted()).isEmpty();

        admin.addToApplied(activity.getId());
        assertThat(activity.getApplied()).isEmpty();

        admin.addToAccepted(activity.getId(), applicant);
        assertThat(activity.getAccepted()).isEmpty();

        applicant.addToApplied(activity.getId());
        assertThat(activity.getApplied()).hasSize(1);

        admin.addToAccepted(activity.getId(), applicant);
        assertThat(activity.getAccepted()).hasSize(1);
        assertThat(activity.getApplied()).hasSize(0);
    }
}
